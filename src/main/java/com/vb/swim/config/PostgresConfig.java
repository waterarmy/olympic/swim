package com.vb.swim.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.boot.autoconfigure.domain.EntityScan;

@Configuration
@EntityScan(basePackages = "com.vb.swim.model")
@EnableJpaRepositories(basePackages = "com.vb.swim.repository")
public class PostgresConfig {
}
