package com.vb.swim.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Player {
  @Id
  @GeneratedValue(strategy=GenerationType.AUTO)
  private int id;
  private final String name;
  private int score;

  public Player(String name) {
    this.name = name;
  }

  public Player(String name, int score) {
    this.name = name;
    this.score = score;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public int getScore() {
    return score;
  }

  public void setScore(int score) {
    this.score = score;
  }

  public void addScore(int score) {
    this.score += score;
  }
}
